<?php
session_start();
$view = $_GET['view'];
$action = $_GET['action'];
switch ($view) {
    case 'login':
        require('controllers/login.php');
        $login = new Login;
        switch ($action) {
            default:
            case 'show':
                $login->show();
                break;
            case 'doLogin':
                $login->doLogin($_POST);
                break;
                case 'logout':
                    $login->logout();
                    break;
        }
        break;
    case 'dashboard':
        switch ($action) {
            default:
            case 'show':
                require('controllers/dashboard.php');
                $dashboard = new Dashboard;
                $dashboard->show();
                break;
        }
        break;
    case 'news':
        switch ($action) {
            default:
            case 'show':
                require('controllers/news.php');
                $news = new News;
                $news->show();
                break;
            case 'new':
                require('controllers/news.php');
                $news = new News;
                $news->new();
                break;
            case 'doCreate':
                require('controllers/news.php');
                $news = new News;
                $news->doCreate($_POST);
                break;
            case 'doUpdate':
                require('controllers/news.php');
                $news = new News;
                $news->doUpdate($_POST);
                break;
            case 'doDelete':
                require('controllers/news.php');
                $news = new News;
                $news->doDelete($_GET['id']);
                break;
        }
        break;
    default:
        break;
}
