<?php
require('helpers/common.php');
class Login {
    public function show() {
        if (CommonHelpers::checkLoggedIn()) {
            CommonHelpers::redirect('route.php?view=dashboard');
        }

        $msg = $_GET['msg'];
        require_once('views/login.php');
        exit();
    }

    public function doLogin($data = array()) {
        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $email = $data['email'];
        $password = $data['password'];
        $sql = 'SELECT * FROM users WHERE email = \'' . $email . '\' AND password = \'' . md5($password) . '\' AND role = 1';
        $result = $connection->query($sql);
        if ($result->num_rows == 0) {
            CommonHelpers::redirect('route.php?view=login&msg=1');
        }

        $_SESSION['user-session'] = $result->fetch_assoc();
        $connection->close();
        CommonHelpers::redirect('route.php?view=dashboard');
    }

    public function logout() {
        $_SESSION['user-session'] = array();
        CommonHelpers::redirect('route.php?view=login');
    }
}