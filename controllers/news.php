<?php
require('helpers/common.php');
class News {
    public function show() {
        if (!CommonHelpers::checkLoggedIn()) {
            CommonHelpers::redirect('route.php?view=login');
        }

        $msg = $_SESSION['flash-msg'];
        $data = array();
        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $sql = 'SELECT * FROM news';
        $result = $connection->query($sql);
        if ($result->num_rows !== 0) {
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }

        $view = 'views/modules/news/news.php';
        require_once('views/index.php');
        $_SESSION['flash-msg'] = '';
        exit();
    }

    public function new() {
        if (!CommonHelpers::checkLoggedIn()) {
            CommonHelpers::redirect('route.php?view=login');
        }

        $msg = $_SESSION['flash-msg'];
        $id = $_GET['id'];
        $action = 'route.php?view=news&action=doCreate';
        if (!empty($id)) {
            $action = 'route.php?view=news&action=doUpdate';
            $data = $this->edit($id);
        }
        $view = 'views/modules/news/new.php';
        require_once('views/index.php');
        $_SESSION['flash-msg'] = '';
        exit();
    }

    public function edit($id = 0) {
        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $sql = 'SELECT * FROM news WHERE id = \'' . $id . '\'';
        $result = $connection->query($sql);
        if ($result->num_rows !== 0) {
            return $result->fetch_assoc();
        }

        return array();
    }

    public function doCreate($data = array()) {
        unset($data['id']);
        $data['datetime'] = date('Y-m-d H:i:s');
        $fields = array();
        $values = array();
        foreach ($data as $key => $value) {
            $fields[] = $key;
            $values[] = "'" . $value . "'";
        }

        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $sql = 'INSERT INTO news (' . implode(',', $fields) . ') VALUES(' . implode(',', $values) . ')';
        $result = $connection->query($sql);
        if ($result === true) {
            $lastID = $connection->insert_id;
            $connection->close();
            $_SESSION['flash-msg'] = 'News ' . $lastID . ' is created successfully!';
            CommonHelpers::redirect('route.php?view=news&action=new&id=' . $lastID);
        }
    }

    public function doUpdate($data = array()) {
        $id = $data['id'];
        unset($data['id']);
        $values = array();
        if (empty($data['status'])) {
            $data['status'] = 0;
        }

        foreach ($data as $key => $value) {
            $values[] = $key . "='" . $value . "'";
        }

        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $sql = 'UPDATE news SET ' . implode(',', $values) . ' WHERE id=' . $id;
        $result = $connection->query($sql);
        if ($result === true) {
            $connection->close();
            $_SESSION['flash-msg'] = 'News ' . $lastID . ' is updated successfully!';
            CommonHelpers::redirect('route.php?view=news&action=new&msg=success&id=' . $id);
        }
    }

    public function doDelete($id = 0) {
        require_once('models/db.php');
        $db = new DB;
        $connection = $db->connect();
        $sql = 'DELETE FROM news WHERE id=' . $id;
        $result = $connection->query($sql);
        if ($result === true) {
            $connection->close();
            $_SESSION['flash-msg'] = 'News ' . $id . ' is removed successfully!';
            CommonHelpers::redirect('route.php?view=news');
        }
    }
}