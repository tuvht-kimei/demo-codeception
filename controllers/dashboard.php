<?php
class Dashboard {
    public function show() {
        require('helpers/common.php');
        if (!CommonHelpers::checkLoggedIn()) {
            CommonHelpers::redirect('route.php?view=login');
        }
        $view = 'views/modules/dashboard/dashboard.php';
        require_once('views/index.php');
        exit();
    }
}