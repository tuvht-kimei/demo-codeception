<?php
class CommonHelpers {
    public static function redirect($url, $permanent = false) {
        if (headers_sent() === false) {
            header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
        }
    
        exit();
    }

    public static function redirectLoggedIn() {
        $userSession = $_SESSION['user-session'];
        if (!$userSession || empty($userSession)) {
            self::redirect('route.php?view=login');
        }

        self::redirect('route.php?view=dashboard');
    }

    public static function checkLoggedIn() {
        $userSession = $_SESSION['user-session'];
        if (!$userSession || empty($userSession)) {
            return false;
        }

        return true;
    }
}
