<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

   /**
     * Function to login to admin LTE
     *
     * @param  string  $username  Username
     * @param  string  $password  Password
     *
     * @return void
     */
    public function loginLTE($email, $password) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$email, $email);
        $I->fillField(\GeneralXpathLibrary::$password, $password);
        $I->click(\GeneralXpathLibrary::$loginButton);
        $I->dontSeeElement(\GeneralXpathLibrary::$errorMsg);
    }

    public function createNews($data) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$title, $data['title']);
        $I->fillField(\GeneralXpathLibrary::$description, $data['description']);
        $I->checkOption(\GeneralXpathLibrary::$status);
        $I->click(\GeneralXpathLibrary::$createButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }

    public function updateNews($data) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$title, $data['title']);
        $I->fillField(\GeneralXpathLibrary::$description, $data['description']);
        $I->checkOption(\GeneralXpathLibrary::$status);
        $I->click(\GeneralXpathLibrary::$createButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }

    public function deleteNews() {
        $I = $this;
        $I->click(\GeneralXpathLibrary::$deleteButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }
}
