<?php
/**
 * @package     Testing
 * @subpackage  Automation Testing Helper
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Helper;
/**
 * Class Helper
 *
 * @since  1.0
 */
class GeneralHelper extends \Codeception\Module
{
    /**
     * Function to check element is not existed
     *
     * @param  string  $element  Element
     *
     * @return boolean
     */
    public function checkElementNotExist($element)
    {
        $I = $this->getModule('PhpBrowser');
        $elementExist = array_filter($I->grabMultiple($element));
        if (empty($elementExist)) {
            return true;
        }

        return false;
    }
}
