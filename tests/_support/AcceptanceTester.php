<?php
/**
 * @package     Testing
 * @subpackage  Automation Testing Helper
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Codeception\Lib\ModuleContainer;
use Codeception\Module\WebDriver;
/**
 * Class Helper
 *
 * @since  1.0
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
     * Function to login to admin LTE
     *
     * @param  string  $username  Username
     * @param  string  $password  Password
     *
     * @return void
     */
    public function loginLTE($email, $password) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$email, $email);
        $I->fillField(\GeneralXpathLibrary::$password, $password);
        $I->click(\GeneralXpathLibrary::$loginButton);
        $I->dontSeeElement(\GeneralXpathLibrary::$errorMsg);
    }

    public function createNews($data) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$title, $data['title']);
        $I->fillField(\GeneralXpathLibrary::$description, $data['description']);
        $I->checkOption(\GeneralXpathLibrary::$status);
        $I->click(\GeneralXpathLibrary::$createButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }

    public function updateNews($data) {
        $I = $this;
        $I->fillField(\GeneralXpathLibrary::$title, $data['title']);
        $I->fillField(\GeneralXpathLibrary::$description, $data['description']);
        $I->checkOption(\GeneralXpathLibrary::$status);
        $I->click(\GeneralXpathLibrary::$createButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }

    public function deleteNews() {
        $I = $this;
        $I->click(\GeneralXpathLibrary::$deleteButton);
        $I->seeElement(\GeneralXpathLibrary::$successMsg);
    }
}
