<?php
/**
 * @package     FE Credit
 * @subpackage  Automation Testing
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Class GeneralXpath
 *
 * @since  1.0
 *
 * @link   http://codeception.com/docs/07-AdvancedUsage#PageObjects
 */
class GeneralXpathLibrary
{
    // Include url of current page
    public static $url = 'http://localhost:8080/';
    public static $dashboardUrl = 'http://localhost:8080/route.php?view=dashboard';
    public static $newsUrl = 'http://localhost:8080/route.php?view=news';
    public static $newsCreateUrl = 'http://localhost:8080/route.php?view=news&action=new';
    // Login page
    public static $email       = '//input[@name="email"]';
    public static $password    = '//input[@name="password"]';
    public static $loginButton = '//button[contains(@class, "login")]';
    public static $errorMsg    = '//div[contains(@class, "alert-danger")]';

    // News
    public static $newsSidebar = '//div[contains(@class, "sidebar")]//ul[contains(@class, "nav-sidebar")]//li//a[contains(@href, "route.php?view=news")]';
    public static $newButton   = '//div[contains(@class, "content-wrapper")]//div[contains(@class, "card-header")]//a[contains(@href, "route.php?view=news&action=new")]';
    public static $title       = '//input[@name="title"]';
    public static $description = '//textarea[@name="description"]';
    public static $status      = '//input[@name="status"]';
    public static $createButton = '//button[contains(@class, "btn-create")]';
    public static $cancelButton = '//a[contains(@class, "btn-cancel")]';
    public static $successMsg   = '//div[contains(@class, "alert-success")]';
    public static $tableRow     = '//div[contains(@class, "content-wrapper")]//div[contains(@class, "card-body")]//tbody//tr';
    public static $deleteButton = '(//div[contains(@class, "content-wrapper")]//div[contains(@class, "card-body")]//tbody//tr//a[contains(@href, "route.php?view=news&action=doDelete")])[1]';
}
