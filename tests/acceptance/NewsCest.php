<?php
/**
 * @package     Testing
 * @subpackage  Automation Testing
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Class News
 *
 * @since  1.0
 */
class NewsCest
{
    /**
     * @var  string
     */
    private $email = 'tuvht@kimei.vn';

    /**
     * @var  string
     */
    private $password = '1234567';

    /**
     * Function to test News
     *
     * @param   AcceptanceTester  $I         Acceptance Tester case.
     * @param   Scenario          $scenario  Scenario for test.
     *
     * @return  void
     */
    public function initNews(AcceptanceTester $I)
    {
        $I->wantTo('Start to test');
        $I->amOnUrl(\GeneralXpathLibrary::$url);
        // $I = new AcceptanceTester\GeneralSteps($I);

        // Login
        $I->wantTo('Login to AdminLTE');
        $I->loginLTE($this->email, $this->password);

        $I->amOnPage(\GeneralXpathLibrary::$dashboardUrl);
        $I->click(\GeneralXpathLibrary::$newsSidebar);
        $I->amOnPage(\GeneralXpathLibrary::$newsUrl);

        $I->click(\GeneralXpathLibrary::$newButton);
        $I->amOnPage(\GeneralXpathLibrary::$newsCreateUrl);

        $createData = array(
            'title' => 'Test Title',
            'description' => 'Test Description'
        );
        $updateData = array(
            'title' => 'Test Title Update',
            'description' => 'Test Description Update'
        );
        // Create.
        $I->createNews($createData);
        // Update
        $I->updateNews($updateData);
        // Redirect to news management
        $I->amOnUrl(\GeneralXpathLibrary::$newsUrl);
        // Delete
        $I->deleteNews();
    }
}
