<div class="col-md-12">
<!-- general form elements -->
    <div class="card card-secondary">
        <div class="card-header">
        <h3 class="card-title">Create news</h3>
        </div>
        <br>
        <?php if (!empty($msg) && $msg !== '') : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo $msg; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="<?php echo $action; ?>" method="post" autocomplete="off">
        <div class="card-body">
            <div class="form-group">
                <label for="news-title">Title</label>
                <input type="text" name="title" class="form-control" id="news-title" placeholder="Enter title" required value="<?php echo !empty($data) ? $data['title'] : ''; ?>">
            </div>
            <div class="form-group">
                <label for="news-description">Description</label>
                <textarea class="form-control" rows="3" id="news-description" placeholder="Enter description" name="description" required><?php echo !empty($data) ? $data['description'] : ''; ?></textarea>
            </div>
            <div class="form-check">
                <input type="checkbox" name="status" class="form-check-input" id="news-status" value="1" <?php echo !empty($data) && $data['status'] == 1 ? 'checked'  : ''; ?>>
                <label class="form-check-label" for="news-status">Enabled</label>
            </div>
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-secondary btn-create"><?php echo !empty($id) ? 'Update' : 'Create'; ?></button>
            <a href="javascript:void(0);" class="btn btn-secondary btn-cancel" onclick="var c = confirm('Do you want to cancel ?'); if (c) window.location.href='route.php?view=news';">Cancel</a>
        </div>
        </form>
    </div>
</div>