<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">News Management</h3><br>
                <a href="route.php?view=news&action=new" class="btn btn-secondary buttons-html5 btn-new">New</a>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
            <?php if (!empty($msg) && $msg !== '') : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo $msg; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
                <?php if (!empty($data)) : ?>
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data as $key => $row) : ?>
                        <tr>
                            <td><?php echo $key + 1; ?></td>
                            <td><?php echo $row['id']; ?></td>
                            <td><a href="route.php?view=news&action=new&id=<?php echo $row['id']; ?>"><?php echo $row['title']; ?></a></td>
                            <td><?php echo date('d-m-Y H:i:s', strtotime($row['datetime'])); ?></td>
                            <td>
                                <?php if ($row['status'] == 1) : ?>
                                    <span class="tag tag-success">Enabled</span>
                                <?php else: ?>
                                    <span class="tag tag-danger">Disabled</span>
                                <?php endif; ?>
                            </td>
                            <td><a href="route.php?view=news&action=doDelete&id=<?php echo $row['id']; ?>" class="btn btn-secondary buttons-html5">Delete</a></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Empty data!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.card -->
    </div>
</div>