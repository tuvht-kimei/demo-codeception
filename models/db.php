<?php
class DB {
    private $servername = '172.18.0.2';
    private $username = 'tuvuong';
    private $password = '123456';
    private $dbname = 'demo_codeception';
    private $port = '3306';

    public function connect() {
        // Create connection
        $connection = new mysqli($this->servername, $this->username, $this->password, $this->dbname, $this->port);
        // Check connection
        if ($connection -> connect_errno) {
            die('Connection failed: ' . $connection->connect_error);
        }

        return $connection;
    }
}

